<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\DataMail;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\DataExport;
use App\Form;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Page
Route::get('/', function () {
    return view('index');
});

Route::get('home', function() {
    return view('index');
})->name('home');

// Sell your Business Page
Route::get('sell-your-business', function() {
    return view('pages.sell_your_business');
})->name('sell-your-business');

// Submit Main Form
Route::post('/submit', 'FormController@submit');

// Notification Page
Route::get('notification/{decision}', 'NotificationController@decision')->name('notification');

