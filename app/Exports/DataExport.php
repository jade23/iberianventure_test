<?php

namespace App\Exports;

use App\Form;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DataExport implements FromQuery, WithHeadings
{

    public function headings(): array
    {
        return [
            'Revenue',
            'Years of growth',
            'Avg. EBITDA last 3 years',
            'Avg. net result last 3 years',
            'Years with positive net results',
            'Net debt',
            'Fixed assets',
            '% biggest shareholder',
            '% revenue from biggest client',
            'Is the company audited? (yes/ no)',
            'm&a in the last 5 years? (yes/ no)',
            'Selling 90%? (yes/ no)',
            'EBITDA/Rev',
            'Net margin',
            'Deuda/EBITDA',
            'Asset to revenue ratio',

        ];
    }

    public function query()
    {
        return Form::query();

    }
}
