<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function decision(Request $request)
    {
        $decision = $request->decision;

        return view('pages.notification_page')->with('decision', $decision);
    }
}
