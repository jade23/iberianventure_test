<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Form;
use Illuminate\Support\Facades\Mail;
use App\Mail\DataMail;
use Maatwebsite\Excel\Facades\Excel;

class FormController extends Controller
{
    public function submit(Request $request) {
            $this->validate($request, [
                // text fields
                'sector' => 'required|string',
                'email' => 'required|email',
                'revenue' => 'required|numeric|min:2',
                'avg_EBITDA_last_3yrs' => 'required|numeric|min:2',
                'avg_net_last_3yrs' => 'required|numeric|min:2',
                'net_debt' => 'required|numeric|min:2',
                'fixed_asset' => 'required|numeric|min:2',
                'biggest_shareholder' => 'required|numeric|between:1,100',
                'revenue_from_biggest_client' => 'required|numeric|between:1,100',
                // Selection Fields
                'mergers_in_5yrs' => 'required',
                'is_audited' => 'required',
                'years_of_growth' => 'required',
                'years_positive_result' => 'required',
                'sell_more_than_90perc' => 'required',
            ],
            [
                'sector.required' => '*Required',

                'email.required' => '*Required',
                'email.email' => 'Must be an valid e-mail address.',

                'revenue.required' => '*Required',
                'revenue.numeric' => 'Must be numeric/numbers',
                'revenue.min' => 'At least two(2) or more digits.',

                'avg_EBITDA_last_3yrs.required' => '*Required',
                'avg_EBITDA_last_3yrs.numeric' => 'Must be numeric/numbers',
                'avg_EBITDA_last_3yrs.min' => 'At least two(2) or more digits.',

                'avg_net_last_3yrs.required' => '*Required',
                'avg_net_last_3yrs.numeric' => 'Must be numeric/numbers',
                'avg_net_last_3yrs.min' => 'At least two(2) or more digits.',

                'net_debt.required' => '*Required',
                'net_debt.numeric' => 'Must be numeric/numbers',
                'net_debt.min' => 'At least two(2) or more digits.',

                'fixed_asset.required' => '*Required',
                'fixed_asset.numeric' => 'Must be numeric/numbers',
                'fixed_asset.min' => 'At least two(2) or more digits.',

                'biggest_shareholder.required' => '*Required',
                'biggest_shareholder.numeric' => 'Must be numeric/numbers',
                'biggest_shareholder.betwwen' => 'Range from 1 to 100 %',

                'revenue_from_biggest_client.required' => '*Required',
                'revenue_from_biggest_client.numeric' => 'Must be numeric/numbers',
                'revenue_from_biggest_client.betwwen' => 'Range from 1 to 100 %',

                'mergers_in_5yrs.required' => 'Please select atleast one.',
                
                'is_audited.required' => 'Please select atleast one.',
                
                'years_of_growth.required' => 'Please select atleast one.',

                'years_positive_result.required' => 'Please select atleast one.',

                'sell_more_than_90perc.required' => 'Please select atleast one.',
            ]
        );

        // Point varable
        $points = 0;
        $decision = '';
        $message = '';

        // Get fields values
        $sector = $request->sector;
        $email = $request->email;
        $revenue = $request->revenue;
        $avg_EBITDA_last_3yrs = $request->avg_EBITDA_last_3yrs;
        $avg_net_last_3yrs = $request->avg_net_last_3yrs;
        $net_debt = $request->net_debt;
        $fixed_asset = $request->fixed_asset;

        $biggest_shareholder = $request->biggest_shareholder;
        $revenue_from_biggest_client = $request->revenue_from_biggest_client;

        
        
        $years_of_growth = $request->years_of_growth;
        $years_positive_result = $request->years_positive_result;
        $is_audited = $request->is_audited;
        $mergers_in_5yrs = $request->mergers_in_5yrs;
        $sell_more_than_90perc = $request->sell_more_than_90perc;
    
        // EBITDA over Revenue percentage
        $ebitda_over_rev = ($avg_EBITDA_last_3yrs / $revenue) * 100;
        // Net Margin
        $net_margin = ($avg_net_last_3yrs / $revenue) * 100;
        // Deuda/EBITDA
        $deuda_over_ebitda = ($net_debt / $avg_EBITDA_last_3yrs);
        // Asset to revenue ratio
        $asset_to_rev_ratio = ($fixed_asset / $revenue);

        // Validation base on company computation
        if($revenue >= 1500000 && $revenue <= 10000000){
            $points++;
        }
        if($years_of_growth >= 3){
            $points++;
        }
        if($avg_EBITDA_last_3yrs >= 150000){
            $points++;
        } else {
            $points = $points - 100;
        }
        if($avg_net_last_3yrs >= 70000){
            $points++;
        }
        if($years_positive_result >= 3 ){
            $points++;
        }
        if($deuda_over_ebitda <= 2) {
            $points++;
        } elseif($deuda_over_ebitda > 3){
            $points = $points - 100;
        }
        if($asset_to_rev_ratio < 1.5){
            $points++;
        }
        if($biggest_shareholder >= 65){
            $points++;
        }
        if($revenue_from_biggest_client >= 40){
            $points++;
        }
        if($is_audited == 1){
            $points++;
        }
        if($mergers_in_5yrs == 1){
            $points++;
        }
        if($sell_more_than_90perc == 1){
            $points++;
        }
        if($ebitda_over_rev >= 7){
            $points++;
        }
        if($net_margin >= 5){
            $points++;
        }

        // Decision
        if($points >= 10){
            // Go
            $decision = '1';
        } else {
            // No-Go
            $decision = '0';
        }

        // Insert Form Data
        $data_form = new Form;
        $data_form->sector = $sector;
        $data_form->email = $email;
        $data_form->revenue = $revenue;
        $data_form->avg_EBITDA_last_3yrs = $avg_EBITDA_last_3yrs;
        $data_form->avg_net_last_3yrs = $avg_net_last_3yrs;
        $data_form->net_debt = $net_debt;
        $data_form->fixed_asset = $fixed_asset;
        $data_form->biggest_shareholder = $biggest_shareholder;
        $data_form->revenue_from_biggest_client = $revenue_from_biggest_client;
        $data_form->mergers_in_5yrs = $mergers_in_5yrs;
        $data_form->is_audited = $is_audited;
        $data_form->years_of_growth = $years_of_growth;
        $data_form->years_positive_result = $years_positive_result;
        $data_form->sell_more_than_90perc = $sell_more_than_90perc;
        $data_form->ebitda_over_rev = $ebitda_over_rev;
        $data_form->net_margin = $net_margin;
        $data_form->deuda_over_ebitda = $deuda_over_ebitda;
        $data_form->asset_to_rev_ratio = $asset_to_rev_ratio;
        $data_form->decision = $decision;
        $data_form->save();


        /*
          Add mail functionality here.
        */
        if($data_form){
            Mail::to('luis@ibventur.es')->send(new DataMail());
            return response()->json(['decision' => $decision], 200);
        } else {
            // error page
        }

    }
}
