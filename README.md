# Iberian Ventures Test

This project was build to showcase the applicant skill-sets.

## Installation

Go to your terminal and run the folowing 

```bash
git clone git clone git@gitlab.com:jade23/iberianventure_test.git
```

Locate the newly created project folder
```bash
cd iberianventure_test
```

Compose
```bash
composer install
```

Copy .env.example
```bash
cp .env.example .env
```

Generate Key
```bash
php artisan key:generate
```

Add Database on your local server name 'iberian_venture'
You should set your username and password too.
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=iberian_venture
DB_USERNAME=root
DB_PASSWORD=
```

Migrate Data tables
```bash
php artisan migrate
```

Run the project
```bash
php artisan serve
```

## Enjoy :) .