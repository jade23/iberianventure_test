@extends('layouts.master')
@section('title','Home')
@section('content')

<body class="landing-page landing-page1">

    {{-- NavBar for our Home page only --}}
    @include('includes.navbar')

    <div class="wrapper" id="app">
        <parallax></parallax>
        <section-features></section-features>
        <section-presentation></section-presentation>
        <footer-vue></footer-vue>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>

@endsection
