@extends('layouts.master')
@section('title','Notification')
@section('content')

    <body class="landing-page landing-page1">

        {{-- NavBar for our Home page only --}}
    @include('includes.navbar')

    <div class="wrapper" id="app">

        @if ($decision == 1)
            <notification_1></notification_1>
            <go></go>
        @elseif($decision == 0)
            <notification_0></notification_0>
            <no_go></no_go>
        @else
            <parallax_error></parallax_error>
            <error_404></error_404>
        @endif

        <section-features></section-features>

        <footer-vue></footer-vue>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    </body>

@endsection
