@extends('layouts.master')
@section('title','Sell Your Business')
@section('content')

    <body class="landing-page landing-page1">

        {{-- NavBar for our Home page only --}}
    @include('includes.navbar')

    <div class="wrapper" id="app">
        <parallax2></parallax2>
        <narrative-section></narrative-section>
        <mainform></mainform>
        <footer-vue></footer-vue>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>

    </body>

@endsection
