/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import VueCurrencyInput from 'vue-currency-input'
 
const pluginOptions = {
    /* see config reference */
    globalOptions: { currency: 'EUR' }
  }
Vue.use(VueCurrencyInput, pluginOptions)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('parallax', require('./components/Parallax.vue').default);
Vue.component('parallax2', require('./components/Parallax2.vue').default);
Vue.component('section-features', require('./components/SectionFeatures.vue').default);
Vue.component('section-presentation', require('./components/SectionPresentation.vue').default);
Vue.component('narrative-section', require('./components/NarrativeSection.vue').default);
Vue.component('mainform', require('./components/forms/MainForm.vue').default);
Vue.component('notification_0', require('./components/Notification_0.vue').default);
Vue.component('no_go', require('./components/message/NoGo.vue').default);
Vue.component('notification_1', require('./components/Notification_1.vue').default);
Vue.component('go', require('./components/message/Go.vue').default);
Vue.component('error_404', require('./components/errors/404.vue').default);
Vue.component('parallax_error', require('./components/errors/ParallaxError.vue').default);
Vue.component('footer-vue', require('./components/Footer.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
