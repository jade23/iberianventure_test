<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sector');
            $table->string('email');
            $table->string('revenue');
            $table->string('years_of_growth');
            $table->string('avg_EBITDA_last_3yrs');
            $table->string('avg_net_last_3yrs');
            $table->integer('years_positive_result');
            $table->string('net_debt');
            $table->string('fixed_asset');
            $table->string('biggest_shareholder');
            $table->string('revenue_from_biggest_client');
            $table->boolean('is_audited');
            $table->boolean('mergers_in_5yrs');
            $table->boolean('sell_more_than_90perc');
            $table->string('ebitda_over_rev');
            $table->string('net_margin');
            $table->string('deuda_over_ebitda');
            $table->string('asset_to_rev_ratio');
            $table->boolean('decision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forms');
    }
}
